﻿using System;
using System.Text;
using Newtonsoft.Json;
using ZeroMQ;

namespace DBZMQ.Infrastructure.ZeroMQ.Common
{
    public static class ZmqSocketExtensions
    {
        public static T ReceiveJson<T>(this ZmqSocket socket) where T : class
        {
            var message = socket.ReceiveMessage();
            var assemblyQualifiedMessageTypeName = Encoding.UTF8.GetString(message.Unwrap().Buffer);
            return JsonConvert.DeserializeObject(
                       Encoding.UTF8.GetString(message.Unwrap().Buffer),
                       Type.GetType(assemblyQualifiedMessageTypeName))
                   as T;
        }

        public static void SendJson<T>(this ZmqSocket socket, T message) where T : class
        {
            var zmqMessage = new ZmqMessage();
            zmqMessage.Push(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));
            zmqMessage.Push(Encoding.UTF8.GetBytes(message.GetType().AssemblyQualifiedName));
            socket.SendMessage(zmqMessage);
        }
    }
}
