﻿using DBZMQ.Client.API.Messages;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.Extensions
{
    internal static class DocumentExtensions
    {
        public static TransportDocument ToTransport(this Document document)
        {
            if (document == null)
                return null;
            return new TransportDocument
            {
                AssemblyQualifiedTypeName = document.AssemblyQualifiedTypeName,
                TypeName = document.TypeName,
                BodyJson = document.BodyJson
            };
        }
    }
}
