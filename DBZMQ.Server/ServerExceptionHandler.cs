﻿using System;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Server
{
    public class ServerExceptionHandler : IHandleExceptions
    {
        private readonly IPublishToTopics _publisher;

        public ServerExceptionHandler(IPublishToTopics publisher)
        {
            _publisher = publisher;
        }

        public void ExceptionOccuredWhileHandling<TMessage>(TMessage message, Exception exception)
            where TMessage : IMessage
        {
            _publisher.Publish(Message.Caused<ServerExceptionOccuredReply>(message, m =>
            {
                m.Exception = exception;
            }));
        }
    }
}
