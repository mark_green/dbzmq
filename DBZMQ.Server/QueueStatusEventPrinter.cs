﻿using DBZMQ.Infrastructure;
using NLog;

namespace DBZMQ.Server
{
    public class QueueStatusEventPrinter : IHandle<QueueStatusEvent>
    {
        public void Handle(QueueStatusEvent message)
        {
            LogManager.GetLogger(string.Format("Queue<{0}>", message.QueueName)).Info("{0} messages pending", message.QueueSize);
        }
    }
}
