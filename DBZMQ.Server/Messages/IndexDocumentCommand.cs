﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Server.Messages
{
    public class IndexDocumentCommand : IMessage
    {
        public string IndexId { get; set; }
        public string DocumentId { get; set; }

        public Guid MessageId { get; set; }
        public Guid CausationId { get; set; }
        public string CorrelationId { get; set; }
    }
}
