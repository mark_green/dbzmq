﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Server.Messages
{
    public class DocumentUpdatedEvent : IMessage
    {
        public string DocumentId { get; set; }

        public Guid MessageId { get; set; }
        public Guid CausationId { get; set; }
        public string CorrelationId { get; set; }
    }
}
