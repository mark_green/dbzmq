﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Infrastructure.Http;
using DBZMQ.Infrastructure.ZeroMQ;
using DBZMQ.Server.Actors;
using DBZMQ.Server.Common;
using DBZMQ.Server.CSScript;
using DBZMQ.Server.Esent;
using DBZMQ.Server.Http;
using DBZMQ.Server.Messages;
using DBZMQ.Server.Policies;
using NLog;
using ZeroMQ;
using ZeroMQ.Devices;

namespace DBZMQ.Server
{
    internal class ServerBootstrapper : SingleThreadWorker
    {
        protected override void Run(Logger logger)
        {
            // ESENT
            var basePath = ConfigurationManager.AppSettings["DBZMQ.Server.Esent.BasePath"];
            if (string.IsNullOrWhiteSpace(basePath))
                basePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "db");
            var documentStore = new StoreDocumentsInEsent(basePath);
            var indexStore = new StoreIndexesInEsent(basePath);
            var collectionStore = new StoreCollectionsInEsent(basePath);

            // CSSCRIPT
            var indexEvaluator = new EvaluateIndexesUsingCSScript();
            var indexTemplateProvider = new ProvideCSScriptIndexDefinitionTemplate();

            // BUS
            var bus = new TopicBasedPubSub();

            // SCHEDULING
            var alarmClock = new AlarmClock(new NowTimeProvider(), bus).WithThreadedQueue();
            bus.Subscribe(alarmClock);

            // ACTORS
            var handleIndexDocumentCommands = LoadBalancer.ComposedOf(bus, 10,
                    () => new HandleIndexDocumentCommands(bus, indexEvaluator, documentStore, collectionStore, indexStore));
            var handleIndexAnyPendingDocumentsCommands = LoadBalancer.ComposedOf(bus, 10,
                    () => new HandleIndexAnyPendingDocumentsCommands(bus, indexStore));
            var replyToGetCollectionIdsRequests = LoadBalancer.ComposedOf(bus, 10,
                    () => new ReplyToGetCollectionIdsRequests(bus, collectionStore)
                            .WithExceptionHandler(() => new ServerExceptionHandler(bus)));
            var replyToGetIndexIdsRequests = LoadBalancer.ComposedOf(bus, 10,
                    () => new ReplyToGetIndexIdsRequests(bus, indexStore)
                            .WithExceptionHandler(() => new ServerExceptionHandler(bus)));
            var replyToGetIndexTemplateRequests =
                          new ReplyToGetIndexTemplateRequests(bus, indexTemplateProvider)
                            .WithExceptionHandler(() => new ServerExceptionHandler(bus))
                            .WithThreadedQueue();
            var replyToReadCollectionRequests = LoadBalancer.ComposedOf(bus, 10,
                    () => new ReplyToReadCollectionRequests(bus, collectionStore, documentStore)
                            .WithExceptionHandler(() => new ServerExceptionHandler(bus)));
            var replyToReadDocumentRequests = LoadBalancer.ComposedOf(bus, 10,
                    () => new ReplyToReadDocumentRequests(bus, documentStore)
                            .WithExceptionHandler(() => new ServerExceptionHandler(bus)));
            var replyToReadIndexRequests = LoadBalancer.ComposedOf(bus, 10,
                    () => new ReplyToReadIndexRequests(bus, indexStore)
                            .WithExceptionHandler(() => new ServerExceptionHandler(bus)));
            var replyToWriteDocumentRequests = LoadBalancer.ComposedOf(bus, 10,
                    () => new ReplyToWriteDocumentRequests(bus, documentStore)
                            .WithExceptionHandler(() => new ServerExceptionHandler(bus)));
            var replyToWriteIndexRequests = LoadBalancer.ComposedOf(bus, 10,
                    () => new ReplyToWriteIndexRequests(bus, documentStore, indexStore, indexEvaluator)
                            .WithExceptionHandler(() => new ServerExceptionHandler(bus)));
            var staleIndexProcessManager =
                new ProcessManagerRunner<IndexUpdatedEvent, ProcessDoesNotEnd>(bus,
                                                                               new StaleIndexProcessManagerFactory
                                                                                   (bus, bus)).WithThreadedQueue();
            var tagForIndexingOnDocumentUpdatedEvent = LoadBalancer.ComposedOf(bus, 10,
                    () => new TagForIndexingOnDocumentUpdatedEvent(bus, documentStore, collectionStore, indexStore));
            
            bus.Subscribe(handleIndexDocumentCommands);
            bus.Subscribe(handleIndexAnyPendingDocumentsCommands);
            bus.Subscribe(replyToGetCollectionIdsRequests);
            bus.Subscribe(replyToGetIndexIdsRequests);
            bus.Subscribe(replyToGetIndexTemplateRequests);
            bus.Subscribe(replyToReadCollectionRequests);
            bus.Subscribe(replyToReadDocumentRequests);
            bus.Subscribe(replyToReadIndexRequests);
            bus.Subscribe(replyToWriteDocumentRequests);
            bus.Subscribe(replyToWriteIndexRequests);
            bus.Subscribe(staleIndexProcessManager);
            bus.Subscribe(tagForIndexingOnDocumentUpdatedEvent);

            // ZEROMQ
            var zmqContext = ZmqContext.Create();
            var zmqQueue = new QueueDevice(zmqContext,
                                        "tcp://*:9292",
                                        "inproc://tcpgateway",
                                        DeviceMode.Threaded);
            zmqQueue.Start();
            var zmqSockets = new List<ZmqSocket>();
            var tcpGateways = Runnable.ComposedOf(20, () =>
            {
                var zmqSocket = zmqContext.CreateSocket(SocketType.REP);
                Retry.UntilSuccessful(() => zmqSocket.Connect("inproc://tcpgateway"), TimeSpan.FromMilliseconds(100));
                zmqSockets.Add(zmqSocket);
                var tcpGateway = new TCPRequestReplyGateway(bus, zmqSocket);
                bus.WideSubscribe(tcpGateway);
                return tcpGateway;
            });

            // HTTP
            var httpBus = new TopicBasedPubSub();
            var httpServer = new HttpServer(httpBus, 9299);
            var httpGateway = new HttpGateway(HttpRoute.SubscribeToReplies(bus, new DocumentGetAdaptor()),
                                              HttpRoute.SubscribeToReplies(bus, new CollectionGetAdaptor()),
                                              HttpRoute.SubscribeToEvents(bus, new QueueStatusEventSubscribeAdaptor()),
                                              HttpRoute.ServeStaticContent(new StaticHtmlAdaptor()),
                                              HttpRoute.ServeStaticContent(new StaticJavascriptAdaptor()));
            httpBus.Subscribe(httpGateway);

            var databaseRunnable = Runnable.ComposedOf(alarmClock,
                                                       handleIndexDocumentCommands,
                                                       handleIndexAnyPendingDocumentsCommands,
                                                       replyToGetCollectionIdsRequests,
                                                       replyToGetIndexIdsRequests,
                                                       replyToGetIndexTemplateRequests,
                                                       replyToReadCollectionRequests,
                                                       replyToReadDocumentRequests,
                                                       replyToReadIndexRequests,
                                                       replyToWriteDocumentRequests,
                                                       replyToWriteIndexRequests,
                                                       staleIndexProcessManager,
                                                       tagForIndexingOnDocumentUpdatedEvent);
            var interfacesRunnable = Runnable.ComposedOf(httpServer, Runnable.ComposedOf(tcpGateways));

            databaseRunnable.Start();
            interfacesRunnable.Start();

            foreach(var indexId in indexStore.AllIds())
                bus.Publish(new IndexUpdatedEvent
                    {
                        IndexId = indexId,
                        CorrelationId = indexId
                    });

            while (Working)
                Thread.Sleep(1000);

            interfacesRunnable.Stop();
            databaseRunnable.Stop();

            foreach(var zmqSocket in zmqSockets)
                zmqSocket.Dispose();
            zmqQueue.Dispose();
            zmqContext.Dispose();
        }
    }
}
