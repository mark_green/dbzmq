﻿using System;
using System.Threading;

namespace DBZMQ.Server.Policies
{
    public static class Retry
    {
        public static void UntilSuccessful(Action action, TimeSpan delay)
        {
            var successful = false;
            do
            {
                try
                {
                    action();
                    successful = true;
                }
                catch (Exception)
                {
                    Thread.Sleep(delay);
                }
            } while (!successful);
        }
    }
}
