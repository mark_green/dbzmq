﻿using System;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Interfaces;

namespace DBZMQ.Server.Actors
{
    public class ReplyToReadIndexRequests :
        IHandle<ReadIndexRequest>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreIndexes _indexStore;

        public ReplyToReadIndexRequests(IPublishToTopics publisher, IStoreIndexes indexStore)
        {
            _publisher = publisher;
            _indexStore = indexStore;
        }

        public void Handle(ReadIndexRequest message)
        {
            var index = _indexStore.Get(message.Id);
            if (index == null)
                throw new Exception(string.Format("Index '{0}' does not exist", message.Id));

            _publisher.Publish(Message.Caused<ReadIndexReply>(message, m =>
            {
                m.Definition = index.Definition;
            }));
        }
    }
}
