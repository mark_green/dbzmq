﻿using System;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Interfaces.Types;
using DBZMQ.Server.Messages;

namespace DBZMQ.Server.Actors
{
    public class ReplyToWriteIndexRequests :
        IHandle<WriteIndexRequest>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreDocuments _documentStore;
        private readonly IStoreIndexes _indexStore;
        private readonly IEvaluateIndexes _indexEvaluator;

        public ReplyToWriteIndexRequests(IPublishToTopics publisher, IStoreDocuments documentStore, IStoreIndexes indexStore, IEvaluateIndexes indexEvaluator)
        {
            _publisher = publisher;
            _documentStore = documentStore;
            _indexStore = indexStore;
            _indexEvaluator = indexEvaluator;
        }

        public void Handle(WriteIndexRequest message)
        {
            var indexId = message.Id;

            var index = new Index
            {
                Id = indexId,
                Definition = message.Definition,
            };
            index.DocumentIdsToEvaluate.UnionWith(_documentStore.AllIds());

            // Test it before keeping it
            try
            {
                _indexEvaluator.GetCollectionIds(index, new Document
                {
                    Id = "TestDocument",
                    TypeName = "TestDocument",
                    AssemblyQualifiedTypeName = "TestDocument",
                    BodyJson = "{Test:\"Document\"}",
                });
            }
            catch (Exception exception)
            {
                throw new ApplicationException("Index evaluation failed. See inner exception", exception);
            }

            _indexStore.Put(index);

            _publisher.Publish(Message.Caused<WriteIndexReply>(message));
            _publisher.Publish(Message.Caused<IndexUpdatedEvent>(message, m =>
                {
                    m.IndexId = indexId;
                    m.CorrelationId = indexId;
                }));
        }
    }
}
