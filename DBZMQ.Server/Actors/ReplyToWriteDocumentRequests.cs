﻿using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Interfaces.Types;
using DBZMQ.Server.Messages;

namespace DBZMQ.Server.Actors
{
    public class ReplyToWriteDocumentRequests :
        IHandle<WriteDocumentRequest>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreDocuments _documentStore;

        public ReplyToWriteDocumentRequests(IPublishToTopics publisher, IStoreDocuments documentStore)
        {
            _publisher = publisher;
            _documentStore = documentStore;
        }


        public void Handle(WriteDocumentRequest message)
        {
            _documentStore.Put(new Document
            {
                Id = message.Id,
                AssemblyQualifiedTypeName = message.Document.AssemblyQualifiedTypeName,
                TypeName = message.Document.TypeName,
                BodyJson = message.Document.BodyJson
            });

            _publisher.Publish(Message.Caused<WriteDocumentReply>(message));
            _publisher.Publish(Message.Caused<DocumentUpdatedEvent>(message, m =>
                {
                    m.DocumentId = message.Id;
                }));
        }
    }
}
