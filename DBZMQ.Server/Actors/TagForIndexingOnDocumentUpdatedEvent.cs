﻿using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Messages;

namespace DBZMQ.Server.Actors
{
    public class TagForIndexingOnDocumentUpdatedEvent :
        IHandle<DocumentUpdatedEvent>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreDocuments _documentStore;
        private readonly IStoreCollections _collectionStore;
        private readonly IStoreIndexes _indexStore;

        public TagForIndexingOnDocumentUpdatedEvent(IPublishToTopics publisher, IStoreDocuments documentStore, IStoreCollections collectionStore, IStoreIndexes indexStore)
        {
            _publisher = publisher;
            _documentStore = documentStore;
            _collectionStore = collectionStore;
            _indexStore = indexStore;
        }

        public void Handle(DocumentUpdatedEvent message)
        {
            var documentId = message.DocumentId;
            _documentStore.Atomic(documentId, document =>
            {
                foreach (var collectionId in document.CollectionIds)
                {
                    _collectionStore.Atomic(collectionId,
                        collection => collection.DocumentIds.Remove(documentId));
                }
                document.CollectionIds.Clear();
            });

            foreach (var indexId in _indexStore.AllIds())
            {
                _indexStore.Atomic(indexId,
                    index =>
                    {
                        index.DocumentIdsToEvaluate.Add(message.DocumentId);
                    });
            }
        }
    }
}
