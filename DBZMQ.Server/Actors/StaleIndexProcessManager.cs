﻿using System;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Messages;

namespace DBZMQ.Server.Actors
{
    public class StaleIndexProcessManagerFactory : IProcessManagerFactory<IndexUpdatedEvent>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IProvideTopicSubscriptions _subscriptionProvider;

        public StaleIndexProcessManagerFactory(IPublishToTopics publisher, IProvideTopicSubscriptions subscriptionProvider)
        {
            _publisher = publisher;
            _subscriptionProvider = subscriptionProvider;
        }

        public IProcessManager New(IndexUpdatedEvent startMessage)
        {
            var pm = new StaleIndexProcessManager(_publisher, startMessage.IndexId);
            _subscriptionProvider.Subscribe(pm);
            return pm;
        }
    }

    public class StaleIndexProcessManager :
        IProcessManager,
        IHandle<IndexStaleCheckDue>
    {
        private readonly IPublishToTopics _publisher;
        private readonly string _indexId;

        public StaleIndexProcessManager(IPublishToTopics publisher, string indexId)
        {
            _publisher = publisher;
            _indexId = indexId;

            ScheduleNextStaleCheck();
        }

        public void Handle(IndexStaleCheckDue message)
        {
            _publisher.Publish(Message.Caused<IndexAnyPendingDocumentsCommand>(message, m =>
                {
                    m.IndexId = _indexId;
                }));

            ScheduleNextStaleCheck();
        }

        private void ScheduleNextStaleCheck()
        {
            _publisher.SendMeIn(TimeSpan.FromSeconds(5),
                new IndexStaleCheckDue
                    {
                        MessageId = Guid.NewGuid(),
                        IndexId = _indexId,
                    });
        }
    }
}
