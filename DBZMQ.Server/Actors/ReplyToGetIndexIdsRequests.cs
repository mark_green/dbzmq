﻿using System.Linq;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Interfaces;

namespace DBZMQ.Server.Actors
{
    public class ReplyToGetIndexIdsRequests :
        IHandle<GetIndexIdsRequest>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreIndexes _indexStore;

        public ReplyToGetIndexIdsRequests(IPublishToTopics publisher, IStoreIndexes indexStore)
        {
            _publisher = publisher;
            _indexStore = indexStore;
        }
        
        public void Handle(GetIndexIdsRequest message)
        {
            _publisher.Publish(Message.Caused<GetIndexIdsReply>(message, m =>
            {
                m.IndexIds = _indexStore.AllIds().Skip(message.Skip).Take(message.Take).ToArray();
            }));
        }
    }
}
