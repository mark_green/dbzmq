﻿using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Interfaces;

namespace DBZMQ.Server.Actors
{
    public class ReplyToGetIndexTemplateRequests : 
        IHandle<GetIndexTemplateRequest>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IProvideTheIndexDefinitionTemplate _indexDefinitionTemplateProvider;

        public ReplyToGetIndexTemplateRequests(IPublishToTopics publisher, IProvideTheIndexDefinitionTemplate indexDefinitionTemplateProvider)
        {
            _publisher = publisher;
            _indexDefinitionTemplateProvider = indexDefinitionTemplateProvider;
        }

        public void Handle(GetIndexTemplateRequest message)
        {
            _publisher.Publish(Message.Caused<GetIndexTemplateReply>(message, m =>
            {
                m.Template = _indexDefinitionTemplateProvider.GetIndexDefinitionTemplate();
            }));
        }
    }
}
