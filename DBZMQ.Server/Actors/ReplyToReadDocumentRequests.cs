﻿using System;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Extensions;
using DBZMQ.Server.Interfaces;

namespace DBZMQ.Server.Actors
{
    public class ReplyToReadDocumentRequests :
        IHandle<ReadDocumentRequest>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreDocuments _documentStore;

        public ReplyToReadDocumentRequests(IPublishToTopics publisher, IStoreDocuments documentStore)
        {
            _publisher = publisher;
            _documentStore = documentStore;
        }

        public void Handle(ReadDocumentRequest message)
        {
            var document = _documentStore.Get(message.Id);
            if (document == null)
                throw new Exception(string.Format("Document '{0}' does not exist", message.Id));

            _publisher.Publish(Message.Caused<ReadDocumentReply>(message, m =>
            {
                m.Document = document.ToTransport();
            }));
        }
    }
}
