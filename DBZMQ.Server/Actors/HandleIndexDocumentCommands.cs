﻿using System.Collections.Generic;
using DBZMQ.Infrastructure;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Interfaces.Types;
using DBZMQ.Server.Messages;

namespace DBZMQ.Server.Actors
{
    public class HandleIndexDocumentCommands
        : IHandle<IndexDocumentCommand>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IEvaluateIndexes _indexEvaluator;
        private readonly IStoreDocuments _documentStore;
        private readonly IStoreCollections _collectionStore;
        private readonly IStoreIndexes _indexStore;

        public HandleIndexDocumentCommands(
            IPublishToTopics publisher,
            IEvaluateIndexes indexEvaluator,
            IStoreDocuments documentStore,
            IStoreCollections collectionStore,
            IStoreIndexes indexStore)
        {
            _publisher = publisher;
            _indexEvaluator = indexEvaluator;
            _documentStore = documentStore;
            _collectionStore = collectionStore;
            _indexStore = indexStore;
        }

        public void Handle(IndexDocumentCommand message)
        {
            var index = _indexStore.Get(message.IndexId);
            var documentCopy = _documentStore.Get(message.DocumentId);
            var collectionIds = _indexEvaluator.GetCollectionIds(index, documentCopy);
            _documentStore.Atomic(message.DocumentId,
                document => document.CollectionIds.UnionWith(collectionIds));
            foreach (var collectionId in collectionIds)
                _collectionStore.Atomic(collectionId,
                    collection => collection.DocumentIds.Add(message.DocumentId),
                    () => new Collection
                    {
                        Id = collectionId,
                        DocumentIds = new HashSet<string>(new[] { message.DocumentId })
                    });
        }
    }
}
