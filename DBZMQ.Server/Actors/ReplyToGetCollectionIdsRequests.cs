﻿using System.Linq;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Interfaces;

namespace DBZMQ.Server.Actors
{
    public class ReplyToGetCollectionIdsRequests : 
        IHandle<GetCollectionIdsRequest>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreCollections _collectionStore;

        public ReplyToGetCollectionIdsRequests(IPublishToTopics publisher, IStoreCollections collectionStore)
        {
            _publisher = publisher;
            _collectionStore = collectionStore;
        }

        public void Handle(GetCollectionIdsRequest message)
        {
            _publisher.Publish(Message.Caused<GetCollectionIdsReply>(message, m =>
            {
                m.CollectionIds = _collectionStore.AllIds().Skip(message.Skip).Take(message.Take).ToArray();
            }));
        }
    }
}
