﻿using System;
using System.Linq;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Extensions;
using DBZMQ.Server.Interfaces;

namespace DBZMQ.Server.Actors
{
    public class ReplyToReadCollectionRequests :
        IHandle<ReadCollectionRequest>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreCollections _collectionStore;
        private readonly IStoreDocuments _documentStore;

        public ReplyToReadCollectionRequests(IPublishToTopics publisher, IStoreCollections collectionStore, IStoreDocuments documentStore)
        {
            _publisher = publisher;
            _collectionStore = collectionStore;
            _documentStore = documentStore;
        }

        public void Handle(ReadCollectionRequest message)
        {
            var collection = _collectionStore.Get(message.Id);
            if (collection == null)
                throw new Exception(string.Format("Collection '{0}' does not exist", message.Id));

            _publisher.Publish(Message.Caused<ReadCollectionReply>(message, m =>
            {
                m.DocumentsById =
                    collection.DocumentIds
                            .Skip(message.Skip)
                            .Take(message.Take)
                            .ToDictionary(
                                id => id,
                                id => _documentStore.Get(id).ToTransport());
            }));
        }
    }
}
