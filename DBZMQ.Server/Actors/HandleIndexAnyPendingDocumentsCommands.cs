﻿using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Messages;

namespace DBZMQ.Server.Actors
{
    public class HandleIndexAnyPendingDocumentsCommands :
        IHandle<IndexAnyPendingDocumentsCommand>
    {
        private readonly IPublishToTopics _publisher;
        private readonly IStoreIndexes _indexStore;

        public HandleIndexAnyPendingDocumentsCommands(IPublishToTopics publisher, IStoreIndexes indexStore)
        {
            _publisher = publisher;
            _indexStore = indexStore;
        }

        public void Handle(IndexAnyPendingDocumentsCommand message)
        {
            var indexId = message.IndexId;
            _indexStore.Atomic(indexId, index =>
            {
                foreach (var documentId in index.DocumentIdsToEvaluate)
                    _publisher.Publish(Message.Caused<IndexDocumentCommand>(message, m =>
                        {
                            m.IndexId = indexId;
                            m.DocumentId = documentId;
                        }));
                index.DocumentIdsToEvaluate.Clear();
            });
        }
    }
}
