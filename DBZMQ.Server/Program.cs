﻿using Topshelf;
using Topshelf.Logging;

namespace DBZMQ.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            HostLogger.UseLogger(new NLogLogWriterFactory.NLogHostLoggerConfigurator());
            HostFactory.Run(x =>
            {
                x.Service<ServerBootstrapper>(s =>
                {
                    s.ConstructUsing(name => new ServerBootstrapper());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("A simple KeyValue DB with a ZeroMQ/TCP API");
                x.SetDisplayName("DB ZeroMQ Server");
                x.SetServiceName("DBZMQ.Server");
            });
        }
    }
}
