﻿using System;
using System.IO;
using System.Text;
using DBZMQ.Client.API;

namespace DBZMQ.Client
{
    public class ConsoleEnteredValue
    {
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Commands:");
            Console.WriteLine("Read:                | R <id>");
            Console.WriteLine("Write:               | W <id> <string>");
            Console.WriteLine("List Collection Ids: | LC");
            Console.WriteLine("Read Collection:     | RC <collectionId>");
            Console.WriteLine("List Index Ids:      | LI");
            Console.WriteLine("Read Index:          | RI <indexId> <filePathToSaveAs>");
            Console.WriteLine("Write Index:         | WI <indexId> <filePathOfDefinition>");
            Console.WriteLine("Get Index Template:  | GIT <filePathToSaveAs>");
            Console.WriteLine();

            using (var context = Context.Builder().NewContext("localhost"))
            {
                var running = true;
                while (running)
                {
                    Console.Write("> ");
                    var command = Console.ReadLine().Split(' ');
                    try
                    {
                        switch (command[0].ToUpper())
                        {
                            case "R":
                                var value = context.ReadDocument<ConsoleEnteredValue>(command[1]);
                                if(value == null)
                                    Console.WriteLine(command[1] + " does not exist");
                                else
                                    Console.WriteLine(command[1] + " = " + value.Value);
                                break;

                            case "W":
                                context.WriteDocument(command[1], new ConsoleEnteredValue { Value = command[2] });
                                Console.WriteLine(command[1] + " = " + command[2]);
                                break;

                            case "LC":
                                foreach (var collectionId in context.GetCollectionIds())
                                    Console.WriteLine(collectionId);
                                break;

                            case "RC":
                                var documents = context.ReadCollection(command[1]);
                                foreach(var document in documents)
                                    Console.WriteLine(document.ToString());
                                break;

                            case "LI":
                                foreach(var indexId in context.GetIndexIds())
                                    Console.WriteLine(indexId);
                                break;

                            case "RI":
                                File.WriteAllText(command[2], context.ReadIndex(command[1]), Encoding.UTF8);
                                Console.WriteLine(command[1] + " saved to disk");
                                break;

                            case "WI":
                                context.WriteIndex(command[1], File.ReadAllText(command[2], Encoding.UTF8));
                                Console.WriteLine(command[1] + " definition updated");
                                break;

                            case "GIT":
                                File.WriteAllText(command[1], context.GetIndexTemplate(), Encoding.UTF8);
                                Console.WriteLine(command[1] + " saved to disk");
                                break;

                            case "EXIT":
                                running = false;
                                break;
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine("Failed: {0}", exception);
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
