﻿using System;

namespace DBZMQ.Client.StressTest
{
    public interface IReceiveStopWatchIntervals
    {
        void IntervalRecorded(TimeSpan duration);
    }

    public class StopWatch : IDisposable
    {
        private readonly IReceiveStopWatchIntervals _intervalRecorder;
        private DateTime _intervalStart;

        public StopWatch(IReceiveStopWatchIntervals intervalRecorder)
        {
            _intervalRecorder = intervalRecorder;
            _intervalStart = DateTime.Now;
        }

        public void Dispose()
        {
            _intervalRecorder.IntervalRecorded(DateTime.Now - _intervalStart);
        }

        public void RecordInterval()
        {
            var nextIntervalStart = DateTime.Now;
            _intervalRecorder.IntervalRecorded(nextIntervalStart - _intervalStart);
            _intervalStart = nextIntervalStart;
        }
    }
}
