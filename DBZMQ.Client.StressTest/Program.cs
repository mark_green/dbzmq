﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DBZMQ.Client.API;

namespace DBZMQ.Client.StressTest
{
    public class Stats
    {
        public int FailedReads { get; set; }
        public int Reads { get; set; }
        public int Writes { get; set; }

        public IntervalAccumulator ReadTime = new IntervalAccumulator();
        public IntervalAccumulator WriteTime = new IntervalAccumulator();
    }

    class Program
    {
        private const int NumberOfThreads = 20;
        private const int NumberOfDocuments = 100;
        private string[] _documentIds;

        readonly IntervalAccumulator _runningTime = new IntervalAccumulator();

        static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            var contextBuilder = Context.Builder();
            var writeContext = contextBuilder.NewContext("localhost");
            _documentIds = new string[NumberOfDocuments];
            for (var i = 0; i < NumberOfDocuments; i++)
            {
                _documentIds[i] = Guid.NewGuid().ToString();

                writeContext.WriteDocument(_documentIds[i], new StressTestValue { Value = _documentIds[i] });
            }
            writeContext.Dispose();

            var running = true;
            var stats = new List<Stats>();

            Console.WriteLine("Press any key to start");
            while (!Console.KeyAvailable)
                Thread.Sleep(100);


            using (var stopWatch = new StopWatch(_runningTime))
            {
                for (var i = 0; i < NumberOfThreads; i++)
                    stats.Add(StartAClient(contextBuilder.NewContext("localhost"), () => running));

                while (Console.KeyAvailable)
                    Console.ReadKey();
                Console.WriteLine("Press any key to stop");
                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(1000);
                    stopWatch.RecordInterval();
                    if (stats.Sum(stat => stat.Writes) > 0)
                        Console.WriteLine("Write rates: {0:0.00} writes/sec - {1:0.000000} sec/write",
                            stats.Sum(stat => (decimal)stat.Writes) / (_runningTime.TotalDuration.Ticks / TimeSpan.TicksPerSecond),
                            stats.Sum(stat => (decimal)stat.WriteTime.TotalDuration.Ticks) / stats.Sum(stat => stat.Writes) / TimeSpan.TicksPerSecond);
                    if (stats.Sum(stat => stat.Reads) > 0)
                        Console.WriteLine(" - {0:0.00} reads/sec - {1:0.000000} sec/read",
                            stats.Sum(stat => (decimal)stat.Reads) / (_runningTime.TotalDuration.Ticks / TimeSpan.TicksPerSecond),
                            stats.Sum(stat => (decimal)stat.ReadTime.TotalDuration.Ticks) / stats.Sum(stat => stat.Reads) / TimeSpan.TicksPerSecond);
                }
                running = false;
            }

            Thread.Sleep(1000);
            Console.WriteLine("==STATS==");
            var thread = 0;
            foreach (var stat in stats)
                Console.WriteLine("#{0} - {1} writes - {2} reads - {3} failed reads", thread++, stat.Writes, stat.Reads, stat.FailedReads);

            Console.WriteLine();

            Console.WriteLine("Total time: {0}", _runningTime.TotalDuration);
            if (stats.Sum(stat => stat.Writes) > 0)
                Console.WriteLine("Write rates: {0:0.00} writes/sec - {1:0.000000} sec/write",
                    stats.Sum(stat => (decimal)stat.Writes) / (_runningTime.TotalDuration.Ticks / TimeSpan.TicksPerSecond),
                    stats.Sum(stat => (decimal)stat.WriteTime.TotalDuration.Ticks) / stats.Sum(stat => stat.Writes) / TimeSpan.TicksPerSecond);
            if(stats.Sum(stat => stat.Reads) > 0)
                Console.WriteLine(" - {0:0.00} reads/sec - {1:0.000000} sec/read",
                    stats.Sum(stat => (decimal)stat.Reads) / (_runningTime.TotalDuration.Ticks / TimeSpan.TicksPerSecond),
                    stats.Sum(stat => (decimal)stat.ReadTime.TotalDuration.Ticks) / stats.Sum(stat => stat.Reads) / TimeSpan.TicksPerSecond);
            Console.WriteLine();

            while (Console.KeyAvailable)
                Console.ReadKey();
            Console.WriteLine("Press any key to exit");
            while (!Console.KeyAvailable)
                Thread.Sleep(100);
        }

        public class StressTestValue
        {
            public string Value { get; set; }
        }

        Stats StartAClient(Context context, Func<bool> keepRunning)
        {
            var stats = new Stats();
            new Thread(() =>
                {
                    var random = new Random();
                    while (keepRunning())
                    {
                        var documentId = _documentIds[random.Next()%NumberOfDocuments];
                        if (true)
                        {
                            try
                            {
                                using(new StopWatch(stats.ReadTime))
                                {
                                    context.ReadDocument<StressTestValue>(documentId);
                                }
                                stats.Reads++;
                            }
                            catch (ServerException)
                            {
                                stats.FailedReads++;
                            }
                        }
                        else
                        {
                            using(new StopWatch(stats.WriteTime))
                            {
                                context.WriteDocument(documentId, new StressTestValue {Value = documentId});
                            }
                            stats.Writes++;
                        }
                    }
                }){IsBackground = true}.Start();
            return stats;
        }
    }

    public class IntervalAccumulator : IReceiveStopWatchIntervals
    {
        private TimeSpan _totalDuration;
        public IntervalAccumulator()
        {
            _totalDuration = TimeSpan.Zero;
        }
        public void IntervalRecorded(TimeSpan duration)
        {
            _totalDuration = _totalDuration.Add(duration);
        }
        public TimeSpan TotalDuration { get { return _totalDuration; } }
    }
}
