﻿using System;

namespace DBZMQ.Client.API
{
    public class ServerException : Exception
    {
        internal ServerException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
