﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Client.API.Messages
{
    public class ReadCollectionRequest : IClientRequestMessage
    {
        public string Id { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }

        public Guid MessageId { get; set; }
        public Guid CausationId { get; set; }
        public string CorrelationId { get; set; }
    }
}
