﻿using System;
using System.Collections.Generic;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Client.API.Messages
{
    public class ReadCollectionReply : IClientReplyMessage
    {
        public Dictionary<string, TransportDocument> DocumentsById { get; set; }

        public Guid MessageId { get; set; }
        public Guid CausationId { get; set; }
        public string CorrelationId { get; set; }
    }
}
