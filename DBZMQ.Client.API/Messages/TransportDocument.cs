﻿namespace DBZMQ.Client.API.Messages
{
    public class TransportDocument
    {
        public string TypeName { get; set; }
        public string AssemblyQualifiedTypeName { get; set; }
        public string BodyJson { get; set; }
    }
}
