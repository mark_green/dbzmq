﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Client.API.Messages
{
    public class GetIndexTemplateRequest : IClientRequestMessage
    {
        public Guid MessageId { get; set; }
        public Guid CausationId { get; set; }
        public string CorrelationId { get; set; }
    }
}
