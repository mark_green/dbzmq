﻿using System;
using System.Linq;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Infrastructure.ZeroMQ.Common;
using Newtonsoft.Json;
using ZeroMQ;

namespace DBZMQ.Client.API
{
    public class Context : IDisposable
    {
        public class ContextBuilder : IDisposable
        {
            private readonly ZmqContext _context;
            internal ContextBuilder()
            {
                _context = ZmqContext.Create();
            }

            public Context NewContext(string address, int port = 9292)
            {
                var socket = _context.CreateSocket(SocketType.REQ);
                socket.Connect(string.Format("tcp://{0}:{1}", address, port));
                return new Context(socket);
            }

            public void Dispose()
            {
                _context.Dispose();
            }
        }

        private static ContextBuilder _builder;

        public static ContextBuilder Builder()
        {
            if (_builder == null)
            {
                _builder = new ContextBuilder();
                return _builder;
            }
            throw new Exception("Only one ContextBuilder is allowed.");
        }

        private readonly ZmqSocket _requestSocket;
        private Context(ZmqSocket requestSocket)
        {
            _requestSocket = requestSocket;
        }

        public void Dispose()
        {
            _requestSocket.Dispose();
        }

        public T ReadDocument<T>(string id) where T :class
        {
            var reply = Request<ReadDocumentReply>(new ReadDocumentRequest
            {
                Id = id,
            });

            return JsonConvert.DeserializeObject<T>(reply.Document.BodyJson);
        }

        public void WriteDocument<T>(string id, T body) where T : class
        {
            Request<WriteDocumentReply>(new WriteDocumentRequest
            {
                Id = id,
                Document = new TransportDocument
                {
                    AssemblyQualifiedTypeName = typeof(T).AssemblyQualifiedName,
                    TypeName = typeof(T).FullName,
                    BodyJson = JsonConvert.SerializeObject(body)
                }
            });
        }

        public object[] ReadCollection(string collectionId, int skip, int take)
        {
            var response = Request<ReadCollectionReply>(new ReadCollectionRequest
            {
                Id = collectionId,

                Skip = skip,
                Take = take,
            });

            return response.DocumentsById.Values
                        .Select(DeserializeTransportDocument)
                        .ToArray();
        }

        public string ReadIndex(string indexId)
        {
            var response = Request<ReadIndexReply>(new ReadIndexRequest
            {
                Id = indexId
            });

            return response.Definition;
        }

        public void WriteIndex(string indexId, string definition)
        {
            var response = Request<WriteIndexReply>(new WriteIndexRequest
            {
                Id = indexId,
                Definition = definition
            });
        }

        public string GetIndexTemplate()
        {
            var response = Request<GetIndexTemplateReply>(new GetIndexTemplateRequest());
            
            return response.Template;
        }

        public string[] GetIndexIds(int skip, int take)
        {
            var response = Request<GetIndexIdsReply>(new GetIndexIdsRequest
            {
                Skip = skip,
                Take = take,
            });

            return response.IndexIds;
        }

        public string[] GetCollectionIds(int skip, int take)
        {
            var response = Request<GetCollectionIdsReply>(new GetCollectionIdsRequest
            {
                Skip = skip,
                Take = take,
            });

            return response.CollectionIds;
        }

        private TReply Request<TReply>(IClientRequestMessage request) where TReply : class, IClientReplyMessage
        {
            request.MessageId = Guid.NewGuid();

            _requestSocket.SendJson(request);
            var response = _requestSocket.ReceiveJson<IClientReplyMessage>();
            if (response is ServerExceptionOccuredReply)
                throw new ServerException("Server exception occured", ((ServerExceptionOccuredReply)response).Exception);
            return response as TReply;
        }

        private static object DeserializeTransportDocument(TransportDocument transportDocument)
        {
            try
            {
                return JsonConvert.DeserializeObject(transportDocument.BodyJson,
                                                     Type.GetType(transportDocument.AssemblyQualifiedTypeName));
            }
            catch
            {
                return JsonConvert.DeserializeObject<dynamic>(transportDocument.BodyJson);
            }
            
        }
    }
}
