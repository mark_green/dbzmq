﻿using System.Collections.Generic;

namespace DBZMQ.Client.API
{
    public static class ContextExtensions
    {
        public static IEnumerable<object> ReadCollection(this Context context, string collectionId)
        {
            const int pageSize = 50;

            var skip = 0;
            object[] currentPage;
            do
            {
                currentPage = context.ReadCollection(collectionId, skip, pageSize);
                foreach (var document in currentPage)
                    yield return document;
                skip += pageSize;
            } while (currentPage.Length == pageSize);
        }

        public static IEnumerable<string> GetIndexIds(this Context context)
        {
            const int pageSize = 50;

            var skip = 0;
            string[] currentPage;
            do
            {
                currentPage = context.GetIndexIds(skip, pageSize);
                foreach (var indexId in currentPage)
                    yield return indexId;
                skip += pageSize;
            } while (currentPage.Length == pageSize);
        }

        public static IEnumerable<string> GetCollectionIds(this Context context)
        {
            const int pageSize = 50;

            var skip = 0;
            string[] currentPage;
            do
            {
                currentPage = context.GetCollectionIds(skip, pageSize);
                foreach (var collectionId in currentPage)
                    yield return collectionId;
                skip += pageSize;
            } while (currentPage.Length == pageSize);
        }
    }
}
