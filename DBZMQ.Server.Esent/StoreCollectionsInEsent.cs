﻿using System;
using System.IO;
using System.Linq;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.Esent
{
    public class StoreCollectionsInEsent : IStoreCollections
    {
        private readonly StoreObjectsInEsent<Collection> _database;

        public StoreCollectionsInEsent(string basePath)
        {
            var path = Path.Combine(basePath, "collections");
            _database = new StoreObjectsInEsent<Collection>(path);
        }

        public void Put(Collection collection)
        {
            _database.Put(collection.Id, collection);
        }

        public Collection Get(string id)
        {
            return _database.Get(id);
        }

        public void Atomic(string id, Action<Collection> alterExisting, Func<Collection> createNew = null)
        {
            _database.Atomic(id, alterExisting, createNew);
        }

        public string[] AllIds()
        {
            return _database.GetAllIds().ToArray();
        }
    }
}
