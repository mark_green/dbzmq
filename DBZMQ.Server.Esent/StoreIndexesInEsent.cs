﻿using System;
using System.IO;
using System.Linq;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.Esent
{
    public class StoreIndexesInEsent : IStoreIndexes
    {
        private readonly StoreObjectsInEsent<Index> _database;

        public StoreIndexesInEsent(string basePath)
        {
            var path = Path.Combine(basePath, "indexes");
            _database = new StoreObjectsInEsent<Index>(path);
        }

        public void Put(Index index)
        {
            _database.Put(index.Id, index);
        }

        public Index Get(string id)
        {
            return _database.Get(id);
        }

        public void Atomic(string id, Action<Index> alterExisting, Func<Index> createNew = null)
        {
            _database.Atomic(id, alterExisting, createNew);
        }

        public string[] AllIds()
        {
            return _database.GetAllIds().ToArray();
        }
    }
}
