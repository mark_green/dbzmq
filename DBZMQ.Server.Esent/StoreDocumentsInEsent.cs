﻿using System;
using System.IO;
using System.Linq;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.Esent
{
    public class StoreDocumentsInEsent : IStoreDocuments
    {
        private readonly StoreObjectsInEsent<Document> _database;

        public StoreDocumentsInEsent(string basePath)
        {
            var path = Path.Combine(basePath, "documents");
            _database = new StoreObjectsInEsent<Document>(path);
        }

        public void Put(Document document)
        {
            _database.Put(document.Id, document);
        }

        public Document Get(string id)
        {
            return _database.Get(id);
        }

        public void Atomic(string id, Action<Document> alterExisting, Func<Document> createNew = null)
        {
            _database.Atomic(id, alterExisting, createNew);
        }

        public string[] AllIds()
        {
            return _database.GetAllIds().ToArray();
        }
    }
}
