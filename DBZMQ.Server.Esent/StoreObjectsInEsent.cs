﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Isam.Esent.Interop;
using Newtonsoft.Json;

namespace DBZMQ.Server.Esent
{
    internal class StoreObjectsInEsent<TObject> : IDisposable where TObject : class
    {
        private const string TableName = "objects";
        private const string IdColumnName = "id";
        private const string BodyColumnName = "body";

        private readonly Instance _instance;
        private readonly string _dbFilePath;

        private const int NumberOfPutLocks = 31;
        private readonly object[] _putLocks;

        public StoreObjectsInEsent(string path)
        {
            _instance = new Instance(typeof(TObject).Name);
            _instance.Parameters.SystemDirectory = path;
            _instance.Parameters.LogFileDirectory = path;
            _instance.Parameters.TempDirectory = path;
            _instance.Parameters.AlternateDatabaseRecoveryDirectory = path;
            _instance.Parameters.CreatePathIfNotExist = true;
            _instance.Init();

            _dbFilePath = Path.Combine(path, string.Format("{0}.db", typeof(TObject).Name));

            using (var session = new Session(_instance))
            {
                try
                {
                    JET_DBID jetDbid;
                    Api.JetCreateDatabase(session, _dbFilePath, null, out jetDbid, CreateDatabaseGrbit.None);
                    using (var transaction = new Transaction(session))
                    {
                        JET_TABLEID tableid;
                        JET_COLUMNID columnid;

                        Api.JetCreateTable(session, jetDbid, TableName, 16, 100, out tableid);

                        var idColumn = new JET_COLUMNDEF { coltyp = JET_coltyp.Text, cp = JET_CP.Unicode };
                        var docColumn = new JET_COLUMNDEF { coltyp = JET_coltyp.LongText, cp = JET_CP.Unicode };

                        Api.JetAddColumn(session, tableid, IdColumnName, idColumn, null, 0, out columnid);
                        Api.JetAddColumn(session, tableid, BodyColumnName, docColumn, null, 0, out columnid);

                        var keyDescriptor = string.Format("+{0}\0\0", IdColumnName);

                        Api.JetCreateIndex(session, tableid, "indexObjectID", CreateIndexGrbit.IndexPrimary, keyDescriptor,
                                            keyDescriptor.Length, 100);

                        Api.JetCloseTable(session, tableid);

                        transaction.Commit(CommitTransactionGrbit.None);
                    }
                }
                catch (EsentDatabaseDuplicateException)
                {
                }
            }

            _putLocks = new object[NumberOfPutLocks];
            for (var i = 0; i < _putLocks.Length; ++i)
            {
                _putLocks[i] = new object();
            }
        }

        private object GetLock(string id)
        {
            return _putLocks[Math.Abs(id.GetHashCode() % NumberOfPutLocks)];
        }

        public TObject Get(string id)
        {
            using (var session = new Session(_instance))
            {
                JET_DBID dbid;

                Api.JetAttachDatabase(session, _dbFilePath, AttachDatabaseGrbit.None);
                Api.JetOpenDatabase(session, _dbFilePath, null, out dbid, OpenDatabaseGrbit.None);

                using (var table = new Table(session, dbid, TableName, OpenTableGrbit.None))
                {
                    using (var transaction = new Transaction(session))
                    {
                        return GetInTransaction(session, table, id);
                    }
                }
            }
        }

        public IEnumerable<string> GetAllIds()
        {
            using (var session = new Session(_instance))
            {
                JET_DBID dbid;

                Api.JetAttachDatabase(session, _dbFilePath, AttachDatabaseGrbit.None);
                Api.JetOpenDatabase(session, _dbFilePath, null, out dbid, OpenDatabaseGrbit.None);

                using (var table = new Table(session, dbid, TableName, OpenTableGrbit.None))
                {
                    using (var transaction = new Transaction(session))
                    {
                        Api.JetSetCurrentIndex(session, table, null);
                        if(Api.TryMoveFirst(session, table))
                        {
                            do
                            {
                            
                                yield return Api.RetrieveColumnAsString(session, table, Api.GetColumnDictionary(session, table)[IdColumnName]);
                            } while (Api.TryMoveNext(session, table));
                        }
                    }
                }
            }
        }

        public void Put(string id, TObject body)
        {
            using (var session = new Session(_instance))
            {
                JET_DBID dbid;

                Api.JetAttachDatabase(session, _dbFilePath, AttachDatabaseGrbit.None);
                Api.JetOpenDatabase(session, _dbFilePath, null, out dbid, OpenDatabaseGrbit.None);

                using (var table = new Table(session, dbid, TableName, OpenTableGrbit.None))
                {
                    lock (GetLock(id))
                    {
                        using (var transaction = new Transaction(session))
                        {
                            PutInTransaction(session, table, id, body);

                            transaction.Commit(CommitTransactionGrbit.None);
                        }
                    }
                }
            }
        }

        public void Atomic(string id, Action<TObject> alterExisting, Func<TObject> createNew)
        {
            using (var session = new Session(_instance))
            {
                JET_DBID dbid;

                Api.JetAttachDatabase(session, _dbFilePath, AttachDatabaseGrbit.None);
                Api.JetOpenDatabase(session, _dbFilePath, null, out dbid, OpenDatabaseGrbit.None);

                using (var table = new Table(session, dbid, TableName, OpenTableGrbit.None))
                {
                    lock (GetLock(id))
                    {
                        using (var transaction = new Transaction(session))
                        {
                            var @object = GetInTransaction(session, table, id);
                            if (@object == null)
                                @object = createNew();
                            else
                                alterExisting(@object);
                            PutInTransaction(session, table, id, @object);

                            transaction.Commit(CommitTransactionGrbit.None);
                        }
                    }
                }
            }
        }

        private static TObject GetInTransaction(Session session, Table table, string id)
        {
            Api.JetSetCurrentIndex(session, table, null);
            Api.MakeKey(session, table, id, Encoding.Unicode, MakeKeyGrbit.NewKey);

            if (!Api.TrySeek(session, table, SeekGrbit.SeekEQ))
                return null;

            var bodyJson = Api.RetrieveColumnAsString(session, table, Api.GetColumnDictionary(session, table)[BodyColumnName]);

            return JsonConvert.DeserializeObject<TObject>(bodyJson);
        }

        private static void PutInTransaction(Session session, Table table, string id, TObject body)
        {
            var columnIds = Api.GetColumnDictionary(session, table);

            Api.JetSetCurrentIndex(session, table, null);
            Api.MakeKey(session, table, id, Encoding.Unicode, MakeKeyGrbit.NewKey);

            if (Api.TrySeek(session, table, SeekGrbit.SeekEQ))
            {
                using (var update = new Update(session, table, JET_prep.Replace))
                {
                    Api.SetColumn(session, table, columnIds[BodyColumnName], JsonConvert.SerializeObject(body), Encoding.Unicode);
                    update.Save();
                }
            }
            else
            {
                using (var update = new Update(session, table, JET_prep.Insert))
                {
                    Api.SetColumn(session, table, columnIds[IdColumnName], id, Encoding.Unicode);
                    Api.SetColumn(session, table, columnIds[BodyColumnName], JsonConvert.SerializeObject(body), Encoding.Unicode);
                    update.Save();
                }
            }
        }

        public void Dispose()
        {
            try
            {
                _instance.Term();
            }
            catch
            {
            }
        }
    }
}
