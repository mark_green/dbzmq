﻿using System.IO;

namespace DBZMQ.Server.Esent
{
    public class Configuration
    {
        private readonly string _basePath;

        internal Configuration(string basePath)
        {
            _basePath = basePath;

            if (!Directory.Exists(_basePath))
                Directory.CreateDirectory(_basePath);
        }

        public string BasePath { get { return _basePath; } }
    }
}
