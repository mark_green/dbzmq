﻿using System;
using System.Text.RegularExpressions;
using DBZMQ.Infrastructure;
using DBZMQ.Infrastructure.Http;

namespace DBZMQ.Server.Http
{
    public class QueueStatusEventSubscribeAdaptor : IHttpSubscribeAdaptor<QueueStatusEvent>
    {
        private readonly static Regex UriPattern = new Regex(@"^/events/queuestatus$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public bool CanHandle(Uri uri)
        {
            return UriPattern.IsMatch(uri.PathAndQuery);
        }

        public string ToJson(QueueStatusEvent message)
        {
            return "{ \"QueueName\":\"" + message.QueueName + "\", \"QueueSize\":" + message.QueueSize + " }";
        }
    }
}
