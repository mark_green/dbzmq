﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Infrastructure.Http;

namespace DBZMQ.Server.Http
{
    public class DocumentGetAdaptor : IHttpRequestReplyAdaptor<ReadDocumentRequest>
    {
        private readonly static Regex UriPattern = new Regex(@"^/docs/([^\?]*).*", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public bool CanHandle(Uri uri, string verb)
        {
            return verb.Equals("GET", StringComparison.InvariantCultureIgnoreCase)
                && UriPattern.IsMatch(uri.PathAndQuery);
        }

        public ReadDocumentRequest CreateRequest(HttpListenerContext context)
        {
            var documentId = UriPattern.Matches(context.Request.Url.PathAndQuery)[0].Groups[1].Captures[0].Value;

            return new ReadDocumentRequest
                {
                    Id = documentId,
                    MessageId = Guid.NewGuid(),
                };
        }

        public void HandleReply(HttpListenerContext context, IClientReplyMessage reply)
        {
            var serverException = reply as ServerExceptionOccuredReply;
            if (serverException != null)
            {
                context.RespondError(serverException.Exception);
                return;
            }

            var readReply = reply as ReadDocumentReply;
            context.Response.AppendHeader("X-DocumentTypeName", readReply.Document.TypeName);
            context.RespondOKWithJson(readReply.Document.BodyJson);
        }
    }
}
