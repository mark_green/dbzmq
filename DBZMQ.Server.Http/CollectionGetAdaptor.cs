﻿using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using DBZMQ.Client.API.Messages;
using DBZMQ.Infrastructure.Common;
using DBZMQ.Infrastructure.Http;

namespace DBZMQ.Server.Http
{
    public class CollectionGetAdaptor : IHttpRequestReplyAdaptor<ReadCollectionRequest>
    {
        private readonly static Regex UriPattern = new Regex(@"^/collections/([^\?]*).*", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public bool CanHandle(Uri uri, string verb)
        {
            return verb.Equals("GET", StringComparison.InvariantCultureIgnoreCase)
                && UriPattern.IsMatch(uri.PathAndQuery);
        }

        public ReadCollectionRequest CreateRequest(HttpListenerContext context)
        {
            var collectionId = UriPattern.Matches(context.Request.Url.PathAndQuery)[0].Groups[1].Captures[0].Value;
            var queryString = new QueryString(context.Request.Url.Query);
            var skip = int.Parse(queryString.GetValue("skip") ?? "0");
            var take = int.Parse(queryString.GetValue("take") ?? "100");

            return new ReadCollectionRequest
            {
                Id = collectionId,
                Skip = skip,
                Take = take,

                MessageId = Guid.NewGuid(),
            };
        }

        public void HandleReply(HttpListenerContext context, IClientReplyMessage reply)
        {
            var serverException = reply as ServerExceptionOccuredReply;
            if (serverException != null)
            {
                context.RespondError(serverException.Exception);
                return;
            }

            var readReply = reply as ReadCollectionReply;
            var result = new StringBuilder("{");
            var delimiter = "";
            foreach (var documentId in readReply.DocumentsById.Keys)
            {
                result.AppendFormat("{0}\"{1}\":{2}", delimiter, documentId, readReply.DocumentsById[documentId].BodyJson);
                delimiter = ",";
            }
            result.Append("}");
            context.RespondOKWithJson(result.ToString());
        }
    }
}
