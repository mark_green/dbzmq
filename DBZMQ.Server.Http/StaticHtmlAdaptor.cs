﻿using System.Collections.Generic;
using System.IO;
using DBZMQ.Infrastructure.Http;

namespace DBZMQ.Server.Http
{
    public class StaticHtmlAdaptor : IHttpStaticContentAdaptor
    {
        public IEnumerable<string> GetPaths()
        {
            yield return "/";
            yield return "/docs";
            yield return "/stats/queues";
        }

        public StaticContent GetContentForPath(string path)
        {
            return new StaticContent
                {
                    ContentType = "text/html",
                    Content = File.ReadAllText(string.Format("StaticContent\\html\\{0}.html", path.Replace("/", "_")))
                };
        }
    }
}
