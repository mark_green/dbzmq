﻿window.GetWebSocketRelative = function (relativeUri) {
    if (!window.WebSocket)
        throw 'WebSockets aren\'t supported by your browser. I can\'t be bothered with old stuff.';

    var absoluteUri;
    if (window.location.protocol === 'https:') {
        absoluteUri = 'wss:';
    } else {
        absoluteUri = 'ws:';
    }
    absoluteUri += '//' + window.location.host;
    absoluteUri += relativeUri;

    return new window.WebSocket(absoluteUri);
}