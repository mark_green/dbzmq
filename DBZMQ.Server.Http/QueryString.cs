﻿using System.Linq;
using System.Text.RegularExpressions;

namespace DBZMQ.Server.Http
{
    public class QueryString
    {
        private static readonly Regex ParamRegex = new Regex("[?&]([^&=]*)=([^&]*)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private readonly MatchCollection _paramMatches;

        public QueryString(string queryString)
        {
            _paramMatches = ParamRegex.Matches(queryString);
        }

        public string GetValue(string paramName)
        {
            foreach (var match in _paramMatches.Cast<Match>())
                if (match.Groups[1].Captures[0].Value == paramName)
                    return match.Groups[2].Captures[0].Value;
            return null;
        }
    }
}
