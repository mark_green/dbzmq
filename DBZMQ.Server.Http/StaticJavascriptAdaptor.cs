﻿using System.Collections.Generic;
using System.IO;
using DBZMQ.Infrastructure.Http;

namespace DBZMQ.Server.Http
{
    public class StaticJavascriptAdaptor : IHttpStaticContentAdaptor
    {
        public IEnumerable<string> GetPaths()
        {
            yield return "/js/highcharts.js";
            yield return "/js/websockets.js";
        }

        public StaticContent GetContentForPath(string path)
        {
            return new StaticContent
            {
                ContentType = "application/javascript",
                Content = File.ReadAllText(string.Format("StaticContent{0}", path.Replace("/","\\")))
            };
        }
    }
}
