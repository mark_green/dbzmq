﻿using System;

namespace DBZMQ.Infrastructure.Common
{
    public interface IMessage
    {
        Guid MessageId { get; set; }
        Guid CausationId { get; set; }
        string CorrelationId { get; set; }
    }

    public static class Message
    {
        public static TMessage Caused<TMessage>(IMessage message, Action<TMessage> init=null) where TMessage : class, IMessage, new()
        {
            var result = new TMessage
            {
                MessageId = Guid.NewGuid(),
                CausationId = message.MessageId,
                CorrelationId = message.CorrelationId,
            };
            if(init != null)
                init(result);
            return result;
        }
    }
}
