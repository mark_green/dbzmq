﻿namespace DBZMQ.Server.Interfaces
{
    public interface IProvideTheIndexDefinitionTemplate
    {
        string GetIndexDefinitionTemplate();
    }
}
