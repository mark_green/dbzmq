﻿using System;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.Interfaces
{
    public interface IStoreIndexes
    {
        void Put(Index index);
        Index Get(string id);
        void Atomic(string id, Action<Index> alterExisting, Func<Index> createNew = null);
        string[] AllIds();
    }
}
