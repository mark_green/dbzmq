﻿namespace DBZMQ.Server.Interfaces
{
    public interface IWorker
    {
        void Start();
        void Stop();
    }
}
