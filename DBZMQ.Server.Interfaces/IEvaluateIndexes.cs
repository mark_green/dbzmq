﻿using System.Collections.Generic;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.Interfaces
{
    public interface IEvaluateIndexes
    {
        ISet<string> GetCollectionIds(Index index, Document document);
    }
}
