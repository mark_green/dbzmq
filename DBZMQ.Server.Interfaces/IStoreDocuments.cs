﻿using System;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.Interfaces
{
    public interface IStoreDocuments
    {
        void Put(Document document);
        Document Get(string id);
        void Atomic(string id, Action<Document> alterExisting, Func<Document> createNew = null);
        string[] AllIds();
    }
}
