﻿using System.Collections.Generic;

namespace DBZMQ.Server.Interfaces.Types
{
    public class Collection
    {
        /// <summary>
        /// The Id (name) of this collection
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Ids of all documents in this collection
        /// </summary>
        public HashSet<string> DocumentIds { get; set; }

        public Collection()
        {
            DocumentIds = new HashSet<string>();
        }
    }
}
