﻿using System.Collections.Generic;

namespace DBZMQ.Server.Interfaces.Types
{
    public class Index
    {
        /// <summary>
        /// The Id (name) of this index
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The javascript which implements the index
        /// </summary>
        public string Definition { get; set; }

        /// <summary>
        /// The Ids of documents which have been updated since this index last saw them. If this is non-zero, the index is stale
        /// </summary>
        public HashSet<string> DocumentIdsToEvaluate { get; set; }

        public Index()
        {
            DocumentIdsToEvaluate = new HashSet<string>();
        }
    }
}