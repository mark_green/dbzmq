﻿using System.Collections.Generic;

namespace DBZMQ.Server.Interfaces.Types
{
    public class Document
    {
        /// <summary>
        /// The Id of the document
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The assembly qualified type name of the object originally serialised.
        /// </summary>
        public string AssemblyQualifiedTypeName { get; set; }

        /// <summary>
        /// The type name of the object originally serialised.
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// The body of the document as Json
        /// </summary>
        public string BodyJson { get; set; }

        /// <summary>
        /// The Ids of all collections this document is currently in
        /// </summary>
        public HashSet<string> CollectionIds { get; set; }

        public Document()
        {
            CollectionIds = new HashSet<string>();
        }
    }
}
