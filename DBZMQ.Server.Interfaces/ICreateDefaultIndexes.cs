﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBZMQ.Server.Interfaces
{
    public interface ICreateDefaultIndexes
    {
        void CreateDefaultIndexes(IStoreIndexes indexStore);
    }
}
