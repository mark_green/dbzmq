﻿using System;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.Interfaces
{
    public interface IStoreCollections
    {
        void Put(Collection collection);
        Collection Get(string id);
        void Atomic(string id, Action<Collection> alterExisting, Func<Collection> createNew = null);
        string[] AllIds();
    }
}
