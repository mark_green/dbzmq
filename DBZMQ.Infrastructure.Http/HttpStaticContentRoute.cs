﻿using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace DBZMQ.Infrastructure.Http
{
    public class StaticContent
    {
        public string ContentType { get; set; }
        public string Content { get; set; }
    }
    public interface IHttpStaticContentAdaptor
    {
        IEnumerable<string> GetPaths();
        StaticContent GetContentForPath(string path);
    }

    public class HttpStaticContentRoute : IHttpRoute
    {
        private readonly IHttpStaticContentAdaptor _adaptor;

        public HttpStaticContentRoute(IHttpStaticContentAdaptor adaptor)
        {
            _adaptor = adaptor;
        }

        public bool Matches(HttpListenerContext context)
        {
            return _adaptor.GetPaths().Contains(context.Request.Url.PathAndQuery);
        }

        public void Dispatch(HttpListenerContext context)
        {
            var content = _adaptor.GetContentForPath(context.Request.Url.PathAndQuery);
            context.Response.AddHeader("Cache-Control", "max-age=0");
            context.RespondOK(content.ContentType, content.Content);
        }
    }
}
