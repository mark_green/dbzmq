﻿using System;
using System.Net;
using System.Text;

namespace DBZMQ.Infrastructure.Http
{
    public class HttpServer : IAmRunnable
    {
        private readonly IPublishToTopics _publisher;
        private readonly HttpListener _listener;
        public HttpServer(IPublishToTopics publisher, int port, params string[] hostnames)
        {
            _publisher = publisher;
            _listener = new HttpListener();
            if(hostnames.Length > 0)
                foreach(var hostname in hostnames)
                    _listener.Prefixes.Add(string.Format("http://{0}:{1}/", hostname, port));
            else
                _listener.Prefixes.Add(string.Format("http://+:{0}/", port));
        }

        private void HandleRequest(HttpListenerContext context)
        {
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.AddHeader("Server", "DBZMQ");

            _publisher.Publish(new HttpRequestReceived
                {
                    MessageId = Guid.NewGuid(),

                    Context = context,
                });
        }

        public void Start()
        {
            _listener.Start();
            _listener.BeginGetContext(GetContext, null);
        }

        private void GetContext(IAsyncResult asyncResult)
        {
            // Have we been stopped in the meantime?
            if (!_listener.IsListening)
                return;

            HandleRequest(_listener.EndGetContext(asyncResult));

            // Come back on the next request
            _listener.BeginGetContext(GetContext, null);
        }

        public void Stop()
        {
            _listener.Stop();
        }
    }
}
