﻿using System;
using System.Net;

namespace DBZMQ.Infrastructure.Http
{
    public interface IHttpRoute
    {
        bool Matches(HttpListenerContext context);
        void Dispatch(HttpListenerContext context);
    }
}
