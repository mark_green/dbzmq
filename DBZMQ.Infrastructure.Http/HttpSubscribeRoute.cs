﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure.Http
{
    public interface IHttpSubscribeAdaptor<in TMessage>
        where TMessage : IMessage
    {
        bool CanHandle(Uri uri);
        string ToJson(TMessage message);
    }

    public class HttpSubscribeRoute<TMessage> :
            IHttpRoute,
            IHandle<TMessage>
        where TMessage : IMessage
    {
        private readonly IHttpSubscribeAdaptor<TMessage> _adaptor;

        private readonly List<HttpListenerWebSocketContext> _webSocketContexts = new List<HttpListenerWebSocketContext>();
        private readonly object _webSocketContextsLock = new object();

        public HttpSubscribeRoute(IHttpSubscribeAdaptor<TMessage> adaptor)
        {
            _adaptor = adaptor;
        }

        public bool Matches(HttpListenerContext context)
        {
            return context.Request.IsWebSocketRequest && _adaptor.CanHandle(context.Request.Url);
        }

        public void Dispatch(HttpListenerContext context)
        {
            lock (_webSocketContextsLock)
            {
                _webSocketContexts.Add(context.AcceptWebSocketAsync(null).Result);
            }
        }

        public void Handle(TMessage message)
        {
            lock (_webSocketContextsLock)
            {
                for (var i = 0; i < _webSocketContexts.Count; i++)
                {
                        if (_webSocketContexts[i].WebSocket.State != WebSocketState.Open ||
                        _webSocketContexts[i].WebSocket.CloseStatus.HasValue)
                        {
                            _webSocketContexts.RemoveAt(i);
                            i--;
                            continue;
                        }
                
                    _webSocketContexts[i].WebSocket.SendAsync(
                        new ArraySegment<byte>(Encoding.UTF8.GetBytes(_adaptor.ToJson(message))),
                        WebSocketMessageType.Text,
                        true, CancellationToken.None);
                }
            }
        }
    }
}
