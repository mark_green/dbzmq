﻿using System;

namespace DBZMQ.Infrastructure.Http
{
    public class HttpGateway : IHandle<HttpRequestReceived>
    {
        private readonly IHttpRoute[] _routes;

        public HttpGateway(params IHttpRoute[] routes)
        {
            _routes = routes;
        }

        public void Handle(HttpRequestReceived message)
        {
            foreach(var route in _routes)
                if (route.Matches(message.Context))
                {
                    try
                    {
                        route.Dispatch(message.Context);
                    }
                    catch (Exception exception)
                    {
                        message.Context.RespondError(exception);
                    }
                    return;
                }

            message.Context.RespondNotFound(string.Format("No routes match {0}", message.Context.Request.Url));
        }
    }
}
