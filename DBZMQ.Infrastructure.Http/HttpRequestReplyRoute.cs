﻿using System;
using System.Collections.Generic;
using System.Net;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure.Http
{
    public interface IHttpRequestReplyAdaptor<out TRequestMessage>
        where TRequestMessage : IClientRequestMessage
    {
        bool CanHandle(Uri uri, string verb);
        TRequestMessage CreateRequest(HttpListenerContext context);
        void HandleReply(HttpListenerContext context, IClientReplyMessage reply);
    }

    public class HttpRequestReplyRoute<TRequestMessage> :
            IHttpRoute,
            IHandle<IClientReplyMessage>
        where TRequestMessage : class, IClientRequestMessage
    {
        private readonly IPublishToTopics _publisher;
        private readonly IHttpRequestReplyAdaptor<TRequestMessage> _adapter;
        private readonly List<PendingRequest> _pendingRequests = new List<PendingRequest>();
        private readonly object _pendingRequestsLock = new object();

        public HttpRequestReplyRoute(IPublishToTopics publisher, IHttpRequestReplyAdaptor<TRequestMessage> adaptor)
        {
            _publisher = publisher;
            _adapter = adaptor;
        }

        public bool Matches(HttpListenerContext context)
        {
            return _adapter.CanHandle(context.Request.Url, context.Request.HttpMethod);
        }

        public void Dispatch(HttpListenerContext context)
        {
            var request = _adapter.CreateRequest(context);
            lock (_pendingRequestsLock)
            {
                _pendingRequests.Add(new PendingRequest
                    {
                        Context = context,
                        MessageId = request.MessageId
                    });
            }
            _publisher.Publish(request);
        }

        public void Handle(IClientReplyMessage message)
        {
            HttpListenerContext context = null;
            for(var i=0; i<_pendingRequests.Count; i++)
            {
                lock(_pendingRequestsLock)
                {
                    if (_pendingRequests[i].MessageId == message.CausationId)
                    {
                        context = _pendingRequests[i].Context;
                        _pendingRequests.RemoveAt(i);
                        break;
                    }
                }
            }
            if (context == null)
                return;

            try
            {
                _adapter.HandleReply(context, message);
            }
            catch (Exception exception)
            {
                context.RespondError(exception);
            }
        }

        private class PendingRequest
        {
            public HttpListenerContext Context { get; set; }
            public Guid MessageId { get; set; }
        }
    }

    public static class HttpRoute
    {
        public static HttpRequestReplyRoute<TRequestMessage> SubscribeToReplies<TRequestMessage>(
            TopicBasedPubSub bus,
            IHttpRequestReplyAdaptor<TRequestMessage> adaptor)
                where TRequestMessage : class, IClientRequestMessage
        {
            var route = new HttpRequestReplyRoute<TRequestMessage>(bus, adaptor);
            bus.WideSubscribe(route);
            return route;
        }

        public static HttpSubscribeRoute<TMessage> SubscribeToEvents<TMessage>(
            TopicBasedPubSub bus,
            IHttpSubscribeAdaptor<TMessage> adaptor)
                where TMessage : class, IMessage
        {
            var route = new HttpSubscribeRoute<TMessage>(adaptor);
            bus.Subscribe(route);
            return route;
        }

        public static HttpStaticContentRoute ServeStaticContent(IHttpStaticContentAdaptor adaptor)
        {
            return new HttpStaticContentRoute(adaptor);
        }
    }
}
