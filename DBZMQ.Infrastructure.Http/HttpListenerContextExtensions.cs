﻿using System;
using System.Net;
using System.Text;

namespace DBZMQ.Infrastructure.Http
{
    public static class HttpListenerContextExtensions
    {
        public static void RespondOKWithJson(this HttpListenerContext context, string content)
        {
            Respond(context, 200, "application/json", content);
        }
        public static void RespondOKWithHtml(this HttpListenerContext context, string content)
        {
            Respond(context, 200, "text/html", content);
        }
        public static void RespondOK(this HttpListenerContext context, string contentType, string content)
        {
            Respond(context, 200, contentType, content);
        }

        public static void RespondError(this HttpListenerContext context, Exception exception)
        {
            Respond(context, 500, "text/html", string.Format("<html><title>Error</title><body>{0}</body></html>", exception));
        }

        public static void RespondNotFound(this HttpListenerContext context, string message)
        {
            Respond(context, 404, "text/html", string.Format("<html><title>Not Found</title><body>{0}</body></html>", message));
        }

        private static void Respond(HttpListenerContext context, int code, string contentType, string content)
        {
            context.Response.StatusCode = code;
            context.Response.ContentType = contentType;

            var responseBuffer = Encoding.UTF8.GetBytes(content);
            context.Response.OutputStream.Write(responseBuffer, 0, responseBuffer.Length);
            context.Response.OutputStream.Flush();

            context.Response.Close();
        }
    }
}
