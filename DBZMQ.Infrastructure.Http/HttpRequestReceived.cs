﻿using System.Net;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure.Http
{
    public class HttpRequestReceived : IMessage
    {
        public HttpListenerContext Context { get; set; }

        public System.Guid MessageId { get; set; }
        public System.Guid CausationId { get; set; }
        public string CorrelationId { get; set; }
    }
}
