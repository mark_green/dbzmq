﻿using System.Collections.Generic;

namespace DBZMQ.Server.CSScript
{
    public interface IIndexDefinition
    {
        void IndexDocument(string id, string assemblyQualifiedTypeName, string typeName, dynamic body, ISet<string> collectionIds);
    }
}
