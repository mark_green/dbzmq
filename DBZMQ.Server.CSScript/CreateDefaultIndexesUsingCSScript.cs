﻿using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Interfaces.Types;

namespace DBZMQ.Server.CSScript
{
    public class CreateDefaultIndexesUsingCSScript : ICreateDefaultIndexes
    {
        public void CreateDefaultIndexes(IStoreIndexes indexStore)
        {
            var indexByType = indexStore.Get("DBZMQ/ByTypeName") ?? new Index { Id = "DBZMQ/ByTypeName" };
            indexByType.Definition = ProvideCSScriptIndexDefinitionTemplate.WrapInIndexDefinitionTemplate("collectionIds.Add(string.Format(\"Types/{0}\", typeName));");
            indexStore.Put(indexByType);
        }
    }
}
