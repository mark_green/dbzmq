﻿using System.Collections.Generic;
using DBZMQ.Server.Interfaces;
using DBZMQ.Server.Interfaces.Types;
using Newtonsoft.Json;

namespace DBZMQ.Server.CSScript
{
    public class EvaluateIndexesUsingCSScript : IEvaluateIndexes
    {
        private readonly Dictionary<string, IIndexDefinition> _compiledDefinitions;
        private readonly object _compiledDefinitionsLock;

        public EvaluateIndexesUsingCSScript()
        {
            _compiledDefinitions = new Dictionary<string, IIndexDefinition>();
            _compiledDefinitionsLock = new object();
        }

        private IIndexDefinition GetCompiledDefinition(string definition)
        {
            IIndexDefinition compiledDefinition;
            lock (_compiledDefinitionsLock)
            {
                if (!_compiledDefinitions.TryGetValue(definition, out compiledDefinition))
                {
                    compiledDefinition = CSScriptLibrary.CSScript.Evaluator.LoadMethod<IIndexDefinition>(definition);
                    _compiledDefinitions.Add(definition, compiledDefinition);
                }
            }
            return compiledDefinition;
        }

        public ISet<string> GetCollectionIds(Index index, Document document)
        {
            var result = new HashSet<string>();

            GetCompiledDefinition(index.Definition)
                .IndexDocument(
                        document.Id,
                        document.AssemblyQualifiedTypeName,
                        document.TypeName,
                        JsonConvert.DeserializeObject<dynamic>(document.BodyJson),
                        result);

            return result;
        }
    }
}
