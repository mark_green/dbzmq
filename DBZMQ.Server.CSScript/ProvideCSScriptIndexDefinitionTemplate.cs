﻿using DBZMQ.Server.Interfaces;

namespace DBZMQ.Server.CSScript
{
    public class ProvideCSScriptIndexDefinitionTemplate : IProvideTheIndexDefinitionTemplate
    {
        public string GetIndexDefinitionTemplate()
        {
            return WrapInIndexDefinitionTemplate("// collectionIds.Add(\"My/Collection/Of/Glory\")");
        }

        public static string WrapInIndexDefinitionTemplate(string definitionBody)
        {
            return
                string.Format(
                    "public void IndexDocument(string id, " +
                "\r\n                          string assemblyQualifiedTypeName, " +
                "\r\n                          string typeName, " +
                "\r\n                          dynamic body, " +
                "\r\n                          System.Collections.Generic.ISet<string> collectionIds)" +
                "\r\n{{" +
                "\r\n     {0}" +
                "\r\n}}", definitionBody);
        }
    }
}
