﻿using System;
using System.Threading;
using DBZMQ.Infrastructure.ZeroMQ.Common;
using NLog;
using ZeroMQ;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure.ZeroMQ
{
    public class TCPRequestReplyGateway :
        IHandle<IClientReplyMessage>,
        IAmRunnable
    {
        private readonly IPublishToTopics _publisher;
        private readonly ZmqSocket _replySocket;
        private readonly Poller _poller;

        private readonly TimeSpan _threadStopWaitTimeout = TimeSpan.FromSeconds(10);
        private readonly ManualResetEventSlim _stopped = new ManualResetEventSlim(true);
        private Thread _thread;
        private volatile bool _stop;

        private readonly object _lockObject = new object();
        private Guid _requestMessageId;
        private IClientReplyMessage _replyMessage;

        public TCPRequestReplyGateway(IPublishToTopics publisher, ZmqSocket replySocket)
        {
            _publisher = publisher;
            _replySocket = replySocket;

            _replySocket.ReceiveReady += (sender, eventArgs) =>
            {
                IClientRequestMessage requestMessage;
                lock (_lockObject)
                {
                    requestMessage = _replySocket.ReceiveJson<IClientRequestMessage>();
                    _requestMessageId = requestMessage.MessageId;
                }
                _publisher.Publish(requestMessage);
            };

            _replySocket.SendReady += (sender, eventArgs) =>
            {
                while (_replyMessage == null)
                    Thread.Sleep(1);
                _replySocket.SendJson(_replyMessage);
                _replyMessage = null;
            };

            _poller = new Poller(new [] {_replySocket});
        }

        public void Handle(IClientReplyMessage message)
        {
            lock (_lockObject)
            {
                if (message.CausationId == _requestMessageId)
                    _replyMessage = message;
            }
        }

        private void Run()
        {
            while (!_stop)
                _poller.Poll(TimeSpan.FromMilliseconds(100));
            _stopped.Set(); //signal that the thread has completed.
        }

        public void Start()
        {
            _stopped.Reset();

            _thread = new Thread(Run) { IsBackground = true, Name = GetType().FullName };
            _thread.Start();

            LogManager.GetCurrentClassLogger().Info("Started");
        }

        public void Stop()
        {
            _stop = true;
            if (!_stopped.Wait(_threadStopWaitTimeout))// wait for thread to signal completion.
                throw new TimeoutException(string.Format("Unable to stop thread"));

            LogManager.GetCurrentClassLogger().Info("Stopped");
        }
    }
}
