﻿using System.Threading;
using DBZMQ.Server.Interfaces;
using NLog;

namespace DBZMQ.Server.Common
{
    public abstract class SingleThreadWorker : IWorker
    {
        private readonly Thread _thread;
        protected bool Working { get; private set; }

        protected SingleThreadWorker()
        {
            _thread = new Thread(BootstrapThread);
        }

        private void BootstrapThread()
        {
            var logger = LogManager.GetLogger(GetType().FullName);
            Run(logger);
        }

        protected abstract void Run(Logger logger);

        public void Start()
        {
            Working = true;
            _thread.Start();
        }

        public void Stop()
        {
            Working = false;
            _thread.Join();
        }
    }
}
