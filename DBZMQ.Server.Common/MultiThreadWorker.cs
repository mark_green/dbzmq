﻿using System.Threading;
using DBZMQ.Server.Interfaces;
using NLog;

namespace DBZMQ.Server.Common
{
    public abstract class MultiThreadWorker : IWorker
    {
        private readonly Thread[] _threads;
        protected bool Working { get; private set; }

        protected MultiThreadWorker(int numberOfThreads)
        {
            _threads = new Thread[numberOfThreads];
            for (var thread = 0; thread < numberOfThreads; thread++)
            {
                var threadNumber = thread;
                _threads[thread] = new Thread(() => BootstrapThread(threadNumber))
                    {
                        IsBackground = true,
                        Name = string.Format("{0} #{1}", GetType().FullName, threadNumber)
                    };
            }
        }

        private void BootstrapThread(int threadNumber)
        {
            var logger = LogManager.GetLogger(string.Format("{0} #{1}", GetType().FullName, threadNumber));
            Run(logger);
        }

        protected abstract void Run(Logger logger);

        public void Start()
        {
            Working = true;
            foreach(var thread in _threads)
                thread.Start();
        }

        public void Stop()
        {
            Working = false;
            foreach (var thread in _threads)
                thread.Join();
        }
    }
}
