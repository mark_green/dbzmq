﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public class ExceptionCatchingHandler<TMessage> : IHandle<TMessage> where TMessage : IMessage
    {
        private readonly IHandle<TMessage> _next;
        private readonly IHandleExceptions _exceptionHandler;
        public ExceptionCatchingHandler(IHandle<TMessage> next, IHandleExceptions exceptionHandler)
        {
            _next = next;
            _exceptionHandler = exceptionHandler;
        }

        public void Handle(TMessage message)
        {
            try
            {
                _next.Handle(message);
            }
            catch (Exception exception)
            {
                _exceptionHandler.ExceptionOccuredWhileHandling<TMessage>(message, exception);
            }
        }
    }
}
