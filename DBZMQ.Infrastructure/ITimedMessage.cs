﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public interface ITimedMessage : IMessage
    {
        DateTime DropDeadDate { get; set; }
    }
}
