﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBZMQ.Infrastructure
{
    public interface IProvideTheTime
    {
        DateTime GetTheTime();
    }
}
