﻿using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public interface INamedQueue
    {
        string Name { get; }
        int Count();
    }

    public interface IQueuedHandler<in TMessage> :
        INamedQueue,
        IHandle<TMessage> where TMessage : IMessage
    {
    }
}
