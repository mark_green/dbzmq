﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public interface IPublishToTopics
    {
        void Publish(string topic, IMessage message);
    }

    public interface IProvideTopicSubscriptions
    {
        void Subscribe<TMessage>(string topic, IHandle<TMessage> consumer) where TMessage : class, IMessage;
        void Unsubscribe<TMessage>(string topic, IHandle<TMessage> consumer) where TMessage : class, IMessage;
    }

    public static class IPublishToTopicsExtensions
    {
        public static void Publish<TMessage>(this IPublishToTopics publisher, TMessage message)
            where TMessage : class, IMessage
        {
            publisher.Publish(message.GetType().FullName, message);
            if(message.CorrelationId != null)
                publisher.Publish(message.CorrelationId, message);
            publisher.Publish(message.CausationId.ToString(), message);
        }

        public static void SendMeIn(this IPublishToTopics publisher, TimeSpan delay, IMessage message)
        {
            publisher.Publish(new ScheduledMessage
                {
                    MessageId = Guid.NewGuid(),
                    DueAt = DateTime.Now.Add(delay),
                    Message = message
                });
        }
    }

    public static class IProvideTopicSubscriptionsExtensions
    {
        public static void Subscribe<TMessage>(this IProvideTopicSubscriptions subscriptionProvider, IHandle<TMessage> handler)
            where TMessage : class, IMessage
        {
            subscriptionProvider.Subscribe<TMessage>(typeof(TMessage).FullName, handler);
        }

        public static void WideSubscribe<TMessage>(this IProvideTopicSubscriptions subscriptionProvider, IHandle<TMessage> handler)
            where TMessage : class, IMessage
        {
            var wideType = typeof(TMessage);
            foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
                foreach (var narrowType in assembly.GetTypes())
                    if(wideType.IsAssignableFrom(narrowType))
                        subscriptionProvider.Subscribe<TMessage>(narrowType.FullName, handler);
        }
    }
}
