﻿using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public interface IProcessManagerFactory<in TStartMessage> where TStartMessage : class, IMessage
    {
        IProcessManager New(TStartMessage startMessage);
    }
}
