﻿using System.Collections.Generic;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public class TopicBasedPubSub : IPublishToTopics, IProvideTopicSubscriptions
    {
        private readonly Dictionary<string, Multiplexer<IMessage>> _multiplexers = new Dictionary<string, Multiplexer<IMessage>>();
        private readonly object _multiplexersLock = new object();

        public void Publish(string topic, IMessage message)
        {
            Multiplexer<IMessage> multiplexer;
            bool topicExists;
            lock (_multiplexersLock)
            {
                topicExists = _multiplexers.TryGetValue(topic, out multiplexer);
            }
            
            if(topicExists)
                multiplexer.Handle(message);
        }

        public void Subscribe<TMessage>(string topic, IHandle<TMessage> handler) where TMessage : class, IMessage
        {
            Multiplexer<IMessage> multiplexer;

            lock (_multiplexersLock)
            {
                if (!_multiplexers.TryGetValue(topic, out multiplexer))
                {
                    multiplexer = new Multiplexer<IMessage>();
                    _multiplexers.Add(topic, multiplexer);
                }
            }

            multiplexer.AddHandler(handler.NarrowTo<IMessage, TMessage>());
        }

        public void Unsubscribe<TMessage>(string topic, IHandle<TMessage> handler) where TMessage : class, IMessage
        {
            Multiplexer<IMessage> multiplexer;
            bool topicExists;
            lock (_multiplexersLock)
            {
                topicExists = _multiplexers.TryGetValue(topic, out multiplexer);
            }
            
            if(topicExists)
                multiplexer.RemoveHandler(handler.NarrowTo<IMessage, TMessage>());
        }
    }
}
