﻿using System;
using System.Collections.Generic;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public interface IHandle<in TMessage> where TMessage : IMessage
    {
        void Handle(TMessage message);
    }

    public static class Handlers
    {
        public static IEnumerable<IHandle<TMessage>> Create<TMessage>(int handlers, Func<IHandle<TMessage>> factory)
             where TMessage : IMessage
        {
            for (var i = 0; i < handlers; i++)
                yield return factory();
        }
    }
}
