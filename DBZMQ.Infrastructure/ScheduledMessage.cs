﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public class ScheduledMessage : IMessage
    {
        public IMessage Message { get; set; }
        public DateTime DueAt { get; set; }

        public Guid MessageId { get; set; }
        public string CorrelationId
        {
            get{ return Message.CorrelationId; }
            set { Message.CorrelationId = value; }
        }
        public Guid CausationId
        {
            get { return Message.CausationId; }
            set { Message.CausationId = value; }
        }
    }

    public static class Scheduling
    {
        public static ScheduledMessage ScheduledMessage(IMessage message, TimeSpan delay)
        {
            return new ScheduledMessage
                {
                    Message = message,
                    DueAt = DateTime.Now.Add(delay)
                };
        }
        public static ScheduledMessage ScheduledMessage(IMessage message, DateTime dueAt)
        {
            return new ScheduledMessage
            {
                Message = message,
                DueAt = dueAt
            };
        }
    }
}
