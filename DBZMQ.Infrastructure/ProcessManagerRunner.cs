﻿using System;
using System.Collections.Generic;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public class ProcessManagerRunner<TStartMessage, TEndMessage> :
            IHandle<IMessage>
        where TStartMessage : class, IMessage
        where TEndMessage : class, IMessage
    {
        private readonly Dictionary<string, IProcessManager> _processManagersByCorrelationId;
        private readonly IProvideTopicSubscriptions _subscriptionProvider;
        private readonly IProcessManagerFactory<TStartMessage> _processManagerFactory;
        public ProcessManagerRunner(IProvideTopicSubscriptions subscriptionProvider, IProcessManagerFactory<TStartMessage> processManagerFactory)
        {
            _processManagersByCorrelationId = new Dictionary<string, IProcessManager>();
            _subscriptionProvider = subscriptionProvider;
            _processManagerFactory = processManagerFactory;

            _subscriptionProvider.Subscribe<TStartMessage>(this);
            _subscriptionProvider.Subscribe<TEndMessage>(this);
        }

        public void Handle(IMessage message)
        {
            if (message is TStartMessage)
            {
                var startMessage = message as TStartMessage;
                var processManager = _processManagerFactory.New(startMessage);
                _processManagersByCorrelationId.Add(startMessage.CorrelationId, processManager);

                if (processManager is IHandle<TStartMessage>)
                {
                    ((IHandle<TStartMessage>)processManager).Handle(startMessage);
                }
            }
            else
            {
                var endMessage = message as TEndMessage;
                var processManager = _processManagersByCorrelationId[endMessage.CorrelationId];
                if (processManager is IHandle<TEndMessage>)
                {
                    ((IHandle<TEndMessage>)processManager).Handle(endMessage);
                }
                _processManagersByCorrelationId.Remove(message.CorrelationId);
            }
        }
    }

    public class ProcessDoesNotEnd : IMessage
    {
        public Guid MessageId { get; set; }
        public Guid CausationId { get; set; }
        public string CorrelationId { get; set; }
    }
}
