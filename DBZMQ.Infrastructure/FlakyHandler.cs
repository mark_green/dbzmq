﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public class FlakyHandler<TMessage> : IHandle<TMessage> where TMessage : class, IMessage
    {
        private readonly IHandle<TMessage> _next;
        private readonly int _timesToFailOutOf10;
        private readonly Random _random;
        public FlakyHandler(IHandle<TMessage> next, int timesToFailOutOf10)
        {
            _next = next;
            _timesToFailOutOf10 = timesToFailOutOf10;
            _random = new Random();
        }

        public void Handle(TMessage message)
        {
            if (_random.Next(0, 11) > _timesToFailOutOf10)
                _next.Handle(message);
        }
    }
}
