﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public class DropOldMessagesHandler<TMessage> : IHandle<TMessage> where TMessage : IMessage
    {
        private readonly IHandle<TMessage> _next;
        public DropOldMessagesHandler(IHandle<TMessage> next)
        {
            _next = next;
        }

        public void Handle(TMessage message)
        {
            var ttlMessage = message as ITimedMessage;
            if (ttlMessage != null && ttlMessage.DropDeadDate <= DateTime.Now)
                return;

            _next.Handle(message);
        }
    }
}
