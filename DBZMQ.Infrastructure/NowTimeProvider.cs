﻿using System;

namespace DBZMQ.Infrastructure
{
    public class NowTimeProvider : IProvideTheTime
    {
        public DateTime GetTheTime()
        {
            return DateTime.Now;
        }
    }
}
