﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public class QueueStatusEvent : IMessage
    {
        public string QueueName { get; set; }
        public int QueueSize { get; set; }

        public Guid MessageId { get; set; }
        public Guid CausationId { get; set; }
        public string CorrelationId { get; set; }
    }
}
