﻿using System;
using System.Collections.Generic;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public static class HandlerExtensions
    {
        public static IHandle<TInput> NarrowTo<TInput, TOutput>(this IHandle<TOutput> handler)
            where TInput : IMessage
            where TOutput : class, TInput
        {
            return new NarrowingHandler<TInput, TOutput>(handler);
        }

        public static IHandle<TMessage> DropOldMessages<TMessage>(this IHandle<TMessage> handler)
            where TMessage : IMessage
        {
            return new DropOldMessagesHandler<TMessage>(handler);
        }

        public static IHandle<TMessage> WithExceptionHandler<TMessage>(this IHandle<TMessage> handler, Func<IHandleExceptions> getExceptionHandler)
            where TMessage : IMessage
        {
            return new ExceptionCatchingHandler<TMessage>(handler, getExceptionHandler());
        }

        public static ThreadedQueuedHandler<TMessage> WithThreadedQueue<TMessage>(this IHandle<TMessage> handler, string name=null)
             where TMessage : IMessage
        {
            return new ThreadedQueuedHandler<TMessage>(handler, name ?? string.Format("{0} Handler", typeof(TMessage).Name));
        }

        public static IEnumerable<ThreadedQueuedHandler<TMessage>> WithThreadedQueues<TMessage>(this IEnumerable<IHandle<TMessage>> handlers, string name=null)
             where TMessage : IMessage
        {
            var i = 0;
            foreach (var handler in handlers)
                yield return new ThreadedQueuedHandler<TMessage>(handler,
                                    string.Format("{0} #{1}",
                                            name ?? string.Format("{0} Handler", typeof (TMessage).Name),
                                            ++i));
        }

        public static LoadBalancingDispatchingHandler<TMessage> LoadBalanced<TMessage>(this IEnumerable<IQueuedHandler<TMessage>> handlers, int maxWorkerQueueSize = int.MaxValue)
            where TMessage : IMessage
        {
            return new LoadBalancingDispatchingHandler<TMessage>(handlers, maxWorkerQueueSize);
        }

        public static RoundRobinDispatchingHandler<TMessage> RoundRobin<TMessage>(this IEnumerable<IHandle<TMessage>> handlers)
            where TMessage : IMessage
        {
            return new RoundRobinDispatchingHandler<TMessage>(handlers);
        }
    }
}
