﻿using System;
namespace DBZMQ.Infrastructure
{
    public class CompositeRunnable : IAmRunnable
    {
        private readonly IAmRunnable[] _runnables;
        public CompositeRunnable(IAmRunnable[] runnables)
        {
            _runnables = runnables;
        }

        public void Start()
        {
            foreach (var runnable in _runnables)
                runnable.Start();
        }

        public void Stop()
        {
            foreach (var runnable in _runnables)
                runnable.Stop();
        }
    }

    public static class Runnable
    {
        public static IAmRunnable ComposedOf(params IAmRunnable[] runnables)
        {
            return new CompositeRunnable(runnables);
        }

        public static IAmRunnable ComposedOf(int number, Func<IAmRunnable> createRunnable)
        {
            var runnables = new IAmRunnable[number];
            for (var i = 0; i < number; i++)
                runnables[i] = createRunnable();
            return ComposedOf(runnables);
        }
    }
}
