﻿using System;
using DBZMQ.Infrastructure.Common;

namespace DBZMQ.Infrastructure
{
    public interface IHandleExceptions
    {
        void ExceptionOccuredWhileHandling<TMessage>(TMessage message, Exception exception)
            where TMessage : IMessage;
    }
}
