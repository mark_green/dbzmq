﻿namespace DBZMQ.Infrastructure
{
    public interface IAmRunnable
    {
        void Start();
        void Stop();
    }
}
